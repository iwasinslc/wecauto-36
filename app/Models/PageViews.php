<?php
namespace App\Models;

use App\Traits\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Class PageViews
 * @package App\Models
 *
 * @property string user_id
 * @property string page_url
 * @property string user_ip
 * @property string get_request
 * @property string post_request
 */
class PageViews extends Model
{
    use ModelTrait;

    /** @var array $fillable */
    protected $fillable = [
        'user_id',
        'page_url',
        'user_ip',
        'get_request',
        'post_request',
    ];

    /**
     * @return mixed
     */
    public static function addRecord()
    {
        return self::create([
            'user_id'  => \Auth::user()->id ?? NULL,
            'page_url' => url()->full(),
            'user_ip'  => $_SERVER['REMOTE_ADDR'],
            'get_request'  => json_encode($_GET),
            'post_request'  => json_encode($_POST),
        ]);
    }
}
