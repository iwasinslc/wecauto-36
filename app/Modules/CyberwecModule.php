<?php
namespace App\Modules;

use GuzzleHttp\Client;

/**
 * Class CyberwecModule
 * @package App\Modules
 */
class CyberwecModule
{
    /** @var string $api */
    private $api = 'https://cyberwec.com/api/';

    /**
     * @param string $method
     * @param string|null $type
     * @param array|null $data
     * @return mixed
     * @throws \Exception
     */
    public function sendRequest(string $method, string $type = null, array $data = null)
    {
        if (null === $type) {
            $type = 'GET';
        }

        if (null === $data) {
            $data = [];
        }

        $client   = new Client();
        $baseUrl  = $this->api;
        $headers  = [
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];
        $verify   = config('app.env') == 'production' ? true : false;
        $params   = [
            'headers' => $headers,
            'verify'  => $verify,
        ];

        if (!empty($data)) {
            $params['form_params'] = $data;
        }

        try {
            $response = $client->request($type, $baseUrl . $method, $params);
        } catch (\Exception $e) {
            throw new \Exception('CyberwecModule API request is failed. ' . $e->getMessage());
        }

        if ($response->getStatusCode() !== 200) {
            throw new \Exception('CyberwecModule API response status is ' . $response->getStatusCode() . ' for method ' . $method);
        }

        $body = json_decode($response->getBody()->getContents());

        return $body;
    }

    public function registerCallback(array $data)
    {
        $data['token'] = config('cyberwec.token');
        $data['source'] = config('cyberwec.source');
        $data['action'] = 'registration';

        return $this->sendRequest('registration', 'POST', $data);
    }

    public function buyLicenseCallback(array $data)
    {
        $data['token'] = config('cyberwec.token');
        $data['source'] = config('cyberwec.source');
        $data['action'] = 'license_buy';

        return $this->sendRequest('license_buy', 'POST', $data);
    }
}