<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param string $type
     * @return mixed
     */
    public function handle(Request $request, Closure $next, string $type = 'key')
    {
        if($request->hasHeader('Api-Key') && $request->header('Api-Key') === config('auth.api.' . $type)) {
            return $next($request);
        }
        abort(403);
    }
}
