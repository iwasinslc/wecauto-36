<?php
namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use PragmaRX\Google2FALaravel\Support\Authenticator;

class Google2FA
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check() || !user()->isGoogle2FaEnabled()||user()->isImpersonated()) {
            return $next($request);
        }

        if (session()->has('2fa_passed')) {
            return $next($request);
        }

        if ($request->has('one_time_password')) {
            $one_time_password = trim($request->one_time_password);

            // Initialise the 2FA class
            $google2fa = app('pragmarx.google2fa');

            if (
                $google2fa->verifyGoogle2FA(user()->google_2fa_code, $one_time_password)
                || (config('app.env') == 'develop' && user()->google_2fa_code == $one_time_password) // Allow only for dev use secret code
            ) {
                session()->put('2fa_passed', true);
                return redirect()->route('profile.profile');
            } else {
                if (session()->has('google_2fa_just_enabled')) {
                    /** @var User $user */
                    $user = \Auth::user();
                    $user->setGoogle2FaDisabled();
                    return redirect()->route('profile.settings')->with('error', __('Wrong code entered first time. Google Authenticator Disabled.'));
                } else {
                    return response()->view('technical.google.2fa', [
                        'error' => __('Wrong code!')
                    ]);
                }
            }
        }

        return response()->view('technical.google.2fa');
    }
}
