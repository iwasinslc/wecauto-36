<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\AuthAttempts;
use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class LoginController
 * @package App\Http\Controllers\Auth
 *
 * @property string redirectTo
 * @property int maxAttempts
 * @property int decayMinutes
 */
class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /*
     * Limiting
     */
    protected $maxAttempts = 0;
    protected $decayMinutes = 0;

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'login';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

        $this->maxAttempts = User::MAX_LOGIN_ATTEMPTS;
        $this->decayMinutes = User::LOGIN_BLOCKING;
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     * @throws
     */
    protected function attemptLogin(Request $request)
    {
        /*
         * Check captcha
         */
        $validator = \Validator::make($request->all(), [
            'captcha' => loginCaptchaCanBeShown() ? 'required|captcha' : '',
        ], [
            'captcha.required' => trans('validation.captcha_required'),
            'captcha.captcha' => trans('validation.captcha_captcha')
        ]);

        /*
         * Send errors about captcha
         */
        if ($validator->fails()) {
            throw ValidationException::withMessages([
                $this->username() => [$validator->errors()->get('captcha')[0]],
            ]);
        }



        /*
         * Trying to authorize user
         */
        if (\Auth::attempt(['email' => $request->login, 'password' => $request->password], $request->filled('remember'))) {
            return redirect($this->redirectTo);
        } elseif (\Auth::attempt(['login' => $request->login, 'password' => $request->password], $request->filled('remember'))) {
            return redirect($this->redirectTo);
        } else {
            return $this->sendFailedLoginResponse($request);
        }
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed $user
     * @return mixed
     * @throws
     */
    protected function authenticated(Request $request, $user)
    {
        $data = [
            'user' => $user,
            'ip' => $_SERVER['REMOTE_ADDR']
        ];
        $user->sendNotification('authorized', $data);

        User::notifyAdminsViaNotificationBot('authorized_all', [
            'login' => $user->login,
            'ip' => $_SERVER['REMOTE_ADDR'],
        ]);
    }

    /**
     * @param Request $request
     * @throws \Throwable
     */
    protected function sendFailedLoginResponse(Request $request)
    {




        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * @throws \Exception
     */
    protected function hasTooManyAttempts()
    {
        /*
         * Increment attempts
         */
        session()->increment('login_attempts');

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.throttle', [
                'minutes' => $this->getDiffInMinutesForBlocked(),
            ])],
        ]);
    }

    /**
     * @return int
     */
    private function getDiffInMinutesForBlocked()
    {
        return Carbon::parse(session('blocked_time'))
            ->addMinutes($this->decayMinutes)
            ->diffInMinutes(now());
    }

    /**
     * @return string
     */
    public static function checkClassExists()
    {
        return 'auth looks ok';
    }
}
