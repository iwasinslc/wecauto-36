<?php
namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\User;

class QueueController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $queueUsers = User::whereNotNull('id_bought_datetime')
            ->orderBy('my_id', 'desc')
            ->paginate(20);

        return view('customer.queue', [
            'queueUsers' => $queueUsers,
        ]);
    }
}
