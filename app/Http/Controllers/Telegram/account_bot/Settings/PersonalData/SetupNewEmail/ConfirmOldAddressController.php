<?php
namespace App\Http\Controllers\Telegram\account_bot\Settings\PersonalData\SetupNewEmail;

use App\Http\Controllers\Controller;
use App\Models\Telegram\TelegramBotEvents;
use App\Models\Telegram\TelegramBotMessages;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramBotScopes;
use App\Models\Telegram\TelegramUsers;
use App\Models\Telegram\TelegramWebhooks;
use App\Models\User;
use App\Modules\Messangers\TelegramModule;

class ConfirmOldAddressController extends Controller
{
    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function index(TelegramWebhooks $webhook,
                          TelegramBots $bot,
                          TelegramBotScopes $scope,
                          TelegramUsers $telegramUser,
                          TelegramBotEvents $event)
    {
        /** @var User $user */
        $user = $telegramUser->user()->first();
        \Log::info('test');
        if (null == $user) {
            return response('ok');
        }

        if ($telegramUser->telegram_user_id != $user->email && !empty($user->email) && $user->isVerifiedEmail()) {
            $lastSentKey    = 'rkt8.change_email_confirmation.u-' . $user->id . '.e-' . $user->email;
            $code           = cache()->remember($lastSentKey, 60, function () {
                return rand(1000, 9999);
            });

            $user->sendEmailNotification('change_email_confirmation_code', [
                'code' => $code,
            ]);

            TelegramModule::setLanguageLocale($telegramUser->language);
            $message = view('telegram.account_bot.settings.email.confirm_old', [
                'webhook' => $webhook,
                'bot' => $bot,
                'scope' => $scope,
                'telegramUser' => $telegramUser,
                'event' => $event,
            ])->render();

            if (config('app.env') == 'develop') {
                \Log::info('Prepared VIEW and message for bot:<hr>' . $message);
            }

            try {
                $telegramInstance = new TelegramModule($bot->keyword);
                $telegramInstance->sendMessage($event->chat_id,
                    $message,
                    'HTML',
                    true,
                    false,
                    null,
                    null,
                    $scope);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response('ok');
            }

            return response('ok');
        }

        app()->call(EnterNewEmailController::class.'@index', [
            'webhook' => $webhook,
            'bot' => $bot,
            'scope' => $scope,
            'telegramUser' => $telegramUser,
            'event' => $event,
        ]);

        return response('ok');
    }

    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function checkAndProcessAnswer(TelegramWebhooks $webhook,
                                          TelegramBots $bot,
                                          TelegramBotScopes $scope,
                                          TelegramUsers $telegramUser,
                                          TelegramBotEvents $event,
                                          TelegramBotMessages $userMessage,
                                          TelegramBotMessages $botRequestMessage)
    {
        /*
         * Validate inputs
         */
        $validator = \Validator::make([
            'code' => $userMessage->message
        ], [
            'code' => 'numeric|min:1000|max:9999',
        ]);

        /** @var User $user */
        $user = $telegramUser->user()->first();

        if (null == $user) {
            return response('ok');
        }

        $code = cache()->get('rkt8.change_email_confirmation.u-'.$user->id.'.e-'.$user->email);

        if ($validator->fails() || $userMessage->message != $code) {
            $this->wrongCode($webhook, $bot, $scope, $telegramUser, $event, $userMessage, $botRequestMessage);
            return response('ok');
        }

        /*
         * Save info about passed code
         */
        $codePassedKey = 'rkt8.change_email_confirmation_passed.u-'.$user->id.'.e-'.$user->email;
        cache()->put($codePassedKey, true, 60);

        /*
         * Answer
         */
        $this->userAnswerSuccess($webhook, $bot, $scope, $telegramUser, $event, $userMessage, $botRequestMessage);
    }

    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function userAnswerSuccess(TelegramWebhooks $webhook,
                                      TelegramBots $bot,
                                      TelegramBotScopes $scope,
                                      TelegramUsers $telegramUser,
                                      TelegramBotEvents $event,
                                      TelegramBotMessages $userMessage,
                                      TelegramBotMessages $botRequestMessage)
    {
        TelegramModule::setLanguageLocale($telegramUser->language);
        $message = view('telegram.account_bot.settings.email.change_email_confirmation_passed', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'userMessage'  => $userMessage,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id, $message, 'HTML', true);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }

        TelegramBotMessages::closeUserScopes($event, $bot);

        /*
         * Initiate: Enter new email
         */
        app()->call(EnterNewEmailController::class.'@index', [
            'webhook' => $webhook,
            'bot' => $bot,
            'scope' => $scope,
            'telegramUser' => $telegramUser,
            'event' => $event,
        ]);
    }

    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @param TelegramBotMessages $userMessage
     * @param TelegramBotMessages $botRequestMessage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function wrongCode(TelegramWebhooks $webhook,
                                     TelegramBots $bot,
                                     TelegramBotScopes $scope,
                                     TelegramUsers $telegramUser,
                                     TelegramBotEvents $event,
                                     TelegramBotMessages $userMessage,
                                     TelegramBotMessages $botRequestMessage)
    {
        TelegramModule::setLanguageLocale($telegramUser->language);
        $message = view('telegram.account_bot.settings.email.change_email_confirm_code_wrong', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id, $message, 'HTML', true);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }
    }
}