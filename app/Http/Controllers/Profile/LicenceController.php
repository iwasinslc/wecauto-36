<?php

namespace App\Http\Controllers\Profile;

use App\Events\NotificationEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\RequestLicences;
use App\Jobs\CyberwecBuyLicenceNotification;
use App\Jobs\HandleRankJob;
use App\Models\Licences;
use App\Models\Transaction;
use App\Models\Wallet;
use Illuminate\Http\Request;

class LicenceController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $licences = Licences::orderBy('price')
            ->with(['currency'])->get();

        return view('profile.licences', [
            'licences' => $licences,
        ]);
    }


    public function buy_licence(RequestLicences $request)
    {
//        if (!user()->hasRole(['root']))
//        {
//            return back()->with('error', __('Function dont working'));
//        }

        $data = cache()->get('protect-exchange-' . getUserId());

        if ($data !== null) {

            return back()->with('error', __('Error'));
        }

        cache()->put('protect-exchange-' . getUserId(), '1', now()->addSeconds(10));
        $user = user();

        /**
         * @var Licences $licence
         */
        $licence = Licences::findOrFail($request->licence_id);
//
//        if (user()->activeLicence()&&$licence->price<=user()->licence->price&&(user()->buyLimit()>1&&user()->sellLimit()>1))
//        {
//            return back()->with('error', __('Error'));
//        }

        /**
         * @var Wallet $wallet
         */
        $wallet = $user->wallets()->find($request->wallet_id);

        if (empty($wallet)) {
            return back()->with('error', __('Balance with selected currency was not found'));
        }

        $amount = $licence->price * rate('USD', $wallet->currency->code);

        if ($wallet->balance < $amount) {
            return back()->with('error', __('Requested amount exceeds the wallet balance'));
        }


        if (!$user->canBuyLicence()&&$licence->id<=$user->licence->id)
        {
            return back()->with('error', __('You already buy this licence'));
        }

        try {

            \DB::beginTransaction();
            try {

                $wallet = $user->wallets()->lockForUpdate()->find($wallet->id);
                $user->licence_id = $licence->id;
                $user->close_at = now()->addDays($licence->duration);
                $user->buy_at = now();

                $user->sell_limit = $licence->sell_amount;
                $user->buy_limit = $licence->buy_amount;
                $user->save();

                Transaction::buy_license($wallet, $amount, $licence->id);
                \DB::commit();
            } catch (\Exception $e) {
                \DB::rollBack();
                return back()->with('error', $e->getMessage());
            }

            $user->checkRank();

            $acc_wallet = user()->getUserWallet('FST');
            $acc_wallet->accrueToPartner($licence->price * rate('USD', 'FST'));

            if ($user->cyber_id) {
                CyberwecBuyLicenceNotification::dispatch($user->cyber_id)
                    ->onQueue(getSupervisorName() . '-high')
                    ->delay(now());
            }


        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }

        return back()->with('success', __('The license has been successfully issued'));
    }

}
