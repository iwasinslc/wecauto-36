<?php
namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\TransactionStatus;
use App\Models\TransactionType;
use App\Models\Wallet;
use App\Models\Withdraw;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;

class OperationsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('profile.operations');
    }

    /**
     * @param null $type
     * @return mixed
     * @throws \Exception
     */
    public function dataTable($type = null)
    {
        $date_from = '';

        $date_to = '';

        if (request()->has('type')&&request()->type!='')
        {
            $type = request()->type;
        }

        if (request()->has('date_from')&&request()->date_from!='')
        {
            $date_from = Carbon::createFromFormat('m/d/Y', request()->date_from )->toDateTimeString();
        }

        if (request()->has('date_to')&&request()->date_to!='')
        {
            $date_to = Carbon::createFromFormat('m/d/Y', request()->date_to )->toDateTimeString();
        }

        $operations = cache()->tags('userAllOperations.' . getUserId())->remember('c.' . getUserId() . '.userOperations.type-' . $type.'.'.$date_from.'.'.$date_to, now()->addMinutes(30), function () use ($type, $date_from, $date_to) {

            $operations = Transaction::where('user_id', getUserId());

            if (null !== $type) {
                $typeId = TransactionType::getByName($type);

                if (null !== $typeId) {
                    $operations = $operations->where('type_id', $typeId->id);
                }
            }

            if ($date_from!='')
            {
                $operations = $operations->where('created_at','>=', $date_from);
            }


            if ($date_to!='')
            {
                $operations = $operations->where('created_at','<=', $date_to);
            }

            return $operations
                ->with('user', 'currency', 'type')
                ->get();
        });

        return DataTables::of($operations)
            ->addColumn('type_name', function ($transaction) {
                return __($transaction->type->name);
            })
            ->addColumn('partner_from', function ($transaction) {
                if ($transaction->type->name != 'partner') {
                    return __('not affiliate');
                }

                $wallet = Wallet::where('id', $transaction->source)->first();

                if (null === $wallet) {
                    return __('source not found');
                }

                return $wallet->user->login;
            })
            ->addColumn('partner_from_tg', function ($transaction) {
                if ($transaction->type->name != 'partner') {
                    return __('not affiliate');
                }

                $wallet = Wallet::where('id', $transaction->source)->first();

                if (null === $wallet) {
                    return __('source not found');
                }

                return $wallet->user->phone;
            })
            ->make(true);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function withdrawsDataTable()
    {
        $date_from = '';
        $date_to = '';

        if (request()->has('date_from')&&request()->date_from!='')
        {
            $date_from = Carbon::createFromFormat('m/d/Y', request()->date_from )->toDateTimeString();
        }

        if (request()->has('date_to')&&request()->date_to!='')
        {
            $date_to = Carbon::createFromFormat('m/d/Y', request()->date_to )->toDateTimeString();
        }

        $operations = cache()->tags('userWithdrawOperations.' . getUserId())->remember('c.' . getUserId() . '.userOperations.type-withdraw.'.$date_from.'.'.$date_to, now()->addMinutes(30), function () use ($date_from, $date_to) {

            $operations = Withdraw::where('user_id', getUserId());

            if ($date_from!='')
            {
                $operations = $operations->where('created_at','>=', $date_from);
            }

            if ($date_to!='')
            {
                $operations = $operations->where('created_at','<=', $date_to);
            }

            return $operations
                ->with('user', 'currency')
                ->get();
        });

        return DataTables::of($operations)
            ->addColumn('status', function($transaction) {
                switch ($transaction['status_id']) {
                    case TransactionStatus::STATUS_CREATED:
                        $status = 'Require confirmation by email';
                        break;
                    case TransactionStatus::STATUS_CONFIRMED_BY_EMAIL:
                        $status = 'Require administration approve';
                        break;
                    case TransactionStatus::STATUS_REJECTED:
                        $status = 'Rejected';
                        break;
                    case TransactionStatus::STATUS_APPROVED:
                        $status = 'Approved';
                        break;
                    case TransactionStatus::STATUS_ERROR:
                        $status = 'Failed';
                        break;
                    default:
                        $status = 'Unknown status';
                }
                return $status;
            })
            ->addColumn('approved', function($transaction) {
                return $transaction['status_id'] == TransactionStatus::STATUS_APPROVED;
            })
            ->make(true);
    }
}
