<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;

class DatatablesRequest extends FormRequest
{
    protected array $columns_matching = [];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * @param array $columns_matching
     */
    public function setColumns(array $columns_matching)
    {
        $this->columns_matching = $columns_matching;
    }

    public function getFiltersWithPaging(): array
    {
        return $this->getFilters() + $this->getPagination();
    }

    /**
     * @return array
     */
    public function getPagination()
    {
        return [
            'page' => $this->getPage(),
            'per_page' => $this->getPerPage(),
        ];
    }

    public function getFilters(): array
    {
        $filters = [];

        // If the request doesn't have columns parameter then return empty collection
        if ($this->isNotFilled('columns')) {
            return $filters;
        }

        foreach ($this->query('columns') as $column) {
            if (! empty($column['search']['value'])) {
                $filters[$this->getColumnName($column)] = trim($column['search']['value']);
            }
        }

        return $filters;
    }

    public function getPerPage(): int
    {
        return $this->query('length') ?? 15;
    }

    public function getPage(): ?int
    {
        return $this->filled('start')
            ? ($this->query('start') / $this->query('length')) + 1
            : 1;
    }

    public function getOrder(string $column = 'created_at', string $direction = 'desc'): array
    {
        if ($this->isNotFilled('order') || empty($this->query('order'))) {
            return [
                'column' => $column,
                'direction' => $direction
            ];
        }

        $columns = $this->query('columns');
        $order = current($this->query('order'));

        return [
            'column' => $this->getColumnName($columns[$order['column']]),
            'direction' => $order['dir']
        ];
    }

    /**
     * @param $column
     * @return string
     */
    private function getColumnName($column): string
    {
        $columnName = $column['name'] ?? $column['data'];
        if (isset($this->columns_matching[$columnName])) { // Это для замены имен колонок если нужно
            $columnName = $this->columns_matching[$columnName];
        }
        return $columnName;
    }
}
