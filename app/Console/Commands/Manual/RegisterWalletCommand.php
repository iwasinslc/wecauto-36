<?php
namespace App\Console\Commands\Manual;

use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Console\Command;

/**
 * Class RegisterWalletCommand
 * @package App\Console\Commands\Manual
 */
class RegisterWalletCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'register:wallets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register wallets command.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var User $user */
        foreach (User::get() as $user) {
            /** @var PaymentSystem $paymentSystem */
            foreach (PaymentSystem::get() as $paymentSystem) {
                /** @var Currency $currency */
                foreach ($paymentSystem->currencies()->get() as $currency) {
                    $checkExistsWallet = $user->wallets()
                        ->where('payment_system_id', $paymentSystem->id)
                        ->where('currency_id', $currency->id)
                        ->count();

                    if (0 == $checkExistsWallet) {
                        Wallet::newWallet($user, $currency, $paymentSystem);
                        $this->comment('Wallet '.$currency->code.' was registered for user '.$user->email);
                    }
                }
            }
        }
    }
}
