<?php

namespace App\Console\Commands\Manual;

use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\Withdraw;
use App\Models\TransactionStatus;
use Illuminate\Console\Command;

class WithdrawSeparationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'withdraw:separate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for moving withdraw transactions to separate table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $type = TransactionType::getByName('withdraw');

        $transactions = Transaction::where('type_id', $type->id)
            ->get();

        if($transactions->count() == 0) return 0;

        \DB::transaction(function () use ($transactions){
            foreach($transactions as $transaction) {
                $insert[] = $transaction->id;
                Withdraw::create([
                    'commission' => $transaction->commission,
                    'user_id' => $transaction->user_id,
                    'currency_id' => $transaction->currency_id,
                    'wallet_id' => $transaction->wallet_id,
                    'payment_system_id' => $transaction->payment_system_id,
                    'amount' => $transaction->amount,
                    'status_id' => $transaction->approved ? TransactionStatus::STATUS_APPROVED : TransactionStatus::STATUS_CREATED,
                    'source'=>$transaction->source,
                    'result'=>$transaction->result,
                    'batch_id'=>$transaction->batch_id,
                    'confirmation_code' => $transaction->confirmation_code ?? null,
                    'confirmation_sent_at' => $transaction->confirmation_sent_at ?? null,
                    'created_at' => $transaction->created_at,
                    'updated_at' => $transaction->updated_at
                ]);
            }
            Transaction::destroy($insert);
        });

        return 0;
    }
}
