<?php


namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Partner",
 *     description="User Partner",
 *     @OA\Xml(
 *         name="Partner"
 *     )
 * )
 */
class Partner
{
    /**
     * @OA\Property(ref="#/components/schemas/User/properties/id")
     */
    public $id;

    /**
     * @OA\Property(ref="#/components/schemas/User/properties/name")
     */
    public $name;

    /**
     * @OA\Property(ref="#/components/schemas/User/properties/login")
     */
    public $login;

    /**
     * @OA\Property(
     *     title="Partner Email",
     *     description="Email of the partner",
     *     example="example@email.com"
     * )
     *
     * @var string
     */
    public $email;
}