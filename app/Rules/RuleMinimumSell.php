<?php
namespace App\Rules;

use App\Models\Wallet;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

/**
 * Class RuleHasPhone
 * @package App\Rules
 */
class RuleMinimumSell implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /**
         * @var Wallet $main_wallet
         */
        $main_wallet = user()->wallets()->find(request()->main_wallet_id);

        if ($main_wallet->currency->code!='USD')
        {
            return true;
        }


        $amount = $value;

        return $amount>=1;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Amount must be more than 1$');
    }
}
