<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommonFieldToOrd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_pieces', function (Blueprint $table) {
            $table->integer('common');
        });

        Schema::table('exchange_orders', function (Blueprint $table) {
            $table->integer('common');
            $table->float('licence_rate', 16,8);
        });

        Schema::table('order_requests', function (Blueprint $table) {
            $table->integer('common');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ord', function (Blueprint $table) {
            //
        });
    }
}
