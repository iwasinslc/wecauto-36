@switch(app()->getLocale())
    @case('ru')
        "language": {
        "url": "/json/Russian.json"
        },
        @break
    @case('po')
        "language": {
        "url": "/json/po.json"
        },
        @break
    @case('ar')
        "language": {
        "url": "/json/ar.json"
        },
        @break
    @case('ind')
        "language": {
        "url": "/json/ind.json"
        },
        @break
    @case('fr')
        "language": {
        "url": "/json/fr.json"
        },
        @break
    @case('sp')
        "language": {
        "url": "/json/sp.json"
        },
        @break
    @case('ch')
        "language": {
        "url": "/json/ch.json"
        },
        @break
    @case('kor')
        "language": {
        "url": "/json/kor.json"
        },
        @break
    @case('de')
        "language": {
        "url": "/json/de.json"
        },
        @break
@endswitch