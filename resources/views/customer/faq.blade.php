@extends('layouts.auth')
@section('title', __('FAQ'))
@section('content')
    <section class="page-preview">
        <div class="container">
            <div class="breadcrumbs"><a href="{{route('customer.main')}}">{!! __('Home') !!}</a><span>/ </span><span>{!! __('FAQ') !!}</span>
            </div>
            <h1 class="page-preview__title">{!! __('FAQ') !!}
            </h1>
        </div>
    </section>


    <section class="faq">
        <div class="container">
            <ul class="accordion">
                @foreach(getFaqsList() as $faq)
                    <li class="accordion__item"><a class="accordion__trigger" data-accordion="trigger"><span class="accordion__trigger-overlay"><span>{{ $faq['title'] }}</span></span></a>
                        <div data-accordion="content-container">
                            <div class="accordion__content typography" data-accordion="content">
                                <p>{{ $faq['text'] }} </p>

                            </div>
                        </div>
                    </li>
                @endforeach

            </ul>
{{--            <div class="faq__bottom"><a class="btn btn--line btn--size-md" href="#">Ask your question</a>--}}
{{--            </div>--}}
        </div>
    </section>

@endsection