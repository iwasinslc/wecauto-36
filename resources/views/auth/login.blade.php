@extends('layouts.auth')
@section('title', __('Sign in'))

@section('content')

    <div class="auth__content">
        <div class="auth-module auth-module--not-content">
            <form class="auth-module__form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <h3 class="auth-module__title">{{__('Sign in')}}
                </h3>
                <!-- .field--error-->
                <div class="field field--row">
                    <label>{{ __('E-Mail or Login') }}</label>
                    <input class="field-stroke" type="text" name="login">
                </div>
                <div class="field field--row">
                    <label>{{ __('Password') }}</label>
                    <input class="field-stroke" type="password" name="password">
                </div>
{{--                <div class="field field--row">--}}
{{--                    <label>Passcode</label>--}}
{{--                    <input class="field-stroke" type="password">--}}
{{--                </div>--}}
                <div class="field field--row">
                    <label>{{ __('Enter captcha code') }}</label>
                    <div class="field__group">
                        <input class="field-stroke" type="text" name="captcha" id="captcha">
                        <div class="captcha" style="cursor: pointer"  onclick="refreshCaptcha()"><?= captcha_img() ?>
                        </div>
                    </div>
                </div>
                <div class="auth-module__buttons">
                    <button class="btn btn--warning btn--size-lg">{{__('Sign in')}}
                    </button>
                    <ul>
                        <li><a href="{{ route('password.request') }}">{{__('Forgot password?')}}</a></li>
                        <li><a href="{{ route('register') }}">{{__('Register')}}</a></li>
                    </ul>
                </div>
            </form>
        </div>
    </div>

@endsection
