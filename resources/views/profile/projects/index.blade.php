@extends('layouts.profile')
@section('title', 'Проекты')
@section('content')
    <!-- inventor begin-->
    <div class="inventor" style="padding-top:30px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @php($invitedPayedPartners = \App\Models\User::where('partner_id', \Auth::user()->my_id)->whereNotNull('id_bought_datetime')->count())

                    @if($invitedPayedPartners < env('MINIMUM_PARTNERS_TO_OFFER_PROJECT', 100))
                        <button class="btn btn-primary" onClick="alert('Для того, чтобы вы смогли предложить проект - вам необходимо пригласить как минимум 100 партнеров, которые купили реф. ссылки.');" style="float:right;">Предложить проект</button>
                    @else
                        <a href="{{ route('profile.projects.create') }}">
                            <button class="btn btn-primary" style="float:right;">Предложить проект</button>
                        </a>
                    @endif
                </div>
            </div>
            <div class="row justify-content-center" style="padding-top:50px;">
                <div class="col-xl-8 col-lg-8">
                    <div class="section-title">
                        <h2 class="add-space">Проекты</h2>
                        <p>Пройдите по ссылкам представленных проектов для голосования клуба VMESTE. Ознакомьтесь подробно с проектами и их маркетинг-планами и проголосуйте что более вам кажется подходящим для клубного входа.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @php($projects = \App\Models\Projects::where('published', 1)->where('verified', 1)->orderBy('created_at', 'desc')->get())

                @if(count($projects) == 0)
                    <p style="font-weight: bold; text-align: center; width:100%;">Пока нет зарегистрированных проектов ..</p>
                @endif

                @foreach($projects as $project)
                <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="single-inventor">
                        <div class="part-img" style="width:100%; height:120px; background: white; text-align: center;">
                            <img src="/project_img/{{ $project->logotype_url }}" alt="{{ $project->name }}" style="height: 120px;">
                        </div>
                        <div class="part-text">
                            <span class="name" style="font-size: 16px;">{{ substr($project->name, 0, 40) }}{{ strlen($project->name) >= 40 ? '...' : '' }}</span>

                            <a href="{{ route('profile.projects.show', ['id' => $project->id]) }}">
                                <button class="btn btn-info">ПРОГОЛОСОВАТЬ</button>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- inventor end -->
    <!-- contact end -->
@endsection