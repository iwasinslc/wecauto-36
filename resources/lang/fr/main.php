<?php
return [
    'emails' => [
        'spam_protection'   => 'Le courriel n\'a pas été envoyé. Règles spam.',
        'sent_successfully' => 'Le courriel a été envoyé avec succès.',
        'unable_to_send'    => 'Impossible d\'envoyer ce courriel',

        'request' => [
            'email_required' => 'Email champs requis',
            'email_max'      => 'La longueur maximale du courriel est de 255 caractères',
            'email_email'    => 'Email format incorrect!',

            'text_required' => 'Texte - champs obligatoires',
            'text_min'      => 'La longueur minimale du texte est de 10 caractères',
        ],
    ],
    'transaction_types' => [
        'enter'      => 'Réapprovisionnement du solde',
        'withdraw'   => 'Retrait de fonds',
        'bonus'      => 'Bonus',
        'partner'    => 'Commission de partenaire',
        'dividend'   => 'Gains sur le dépôt',
        'create_dep' => 'Création du dépôt',
        'close_dep'  => 'Clôture du dépôt',
        'penalty'    => 'Amende',
    ],

];
